import pygame
from pygame.locals import *
from Utilities import imageFile
    
class Card(object):
    def __init__(self, name='card-troop', shrink_percent=14):
        self.name = name
        #.convert() converts the image to a pixel format, which speeds up animations
        self.full_res = pygame.image.load(imageFile(name)).convert() 
        self.image = self.full_res.copy()
        self.full_width = self.image.get_rect().size[0]
        self.full_height = self.image.get_rect().size[1]
        self.shrink(shrink_percent)

    def shrink(self, percent):
        percent /= 100.0
        self.image = self.full_res.copy()
        self.image = pygame.transform.scale(self.image, (int(self.full_width*percent), int(self.full_height*percent)))
        self.width = self.image.get_rect().size[0]
        self.height = self.image.get_rect().size[1]