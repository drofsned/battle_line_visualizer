from Utilities import EDGE_MARGIN

class Row_Info(object):
    def __init__(self, num_cards):
        self.width = 0
        self.height = 0
        self.num_cards = num_cards
        self.x_margin = 0
        self.x_pos = []
        self.y_pos = EDGE_MARGIN
    
    def set_width(self, width):
        self.width = width
    
    def set_x_margin(self, margin):
        self.x_margin = margin
    
    def set_num_cards(self, num):
        self.num_cards = num
    
    def __get_region_width(self):
        return self.num_cards*(self.width+self.x_margin) - self.x_margin
    
    def __get_region_x_offset(self):
        return -1*int(self.__get_region_width()/2)
    
    def __get_screen_offset(self, half_width):
        return self.__get_region_x_offset()+half_width
    
    def set_x_pos(self, half_screen_width):
        self.x_pos = []
        for n in xrange(self.num_cards):
            self.x_pos.append(n*(self.width+self.x_margin)+self.__get_screen_offset(half_screen_width))
    
    def set_y_pos(self, pos):
        self.y_pos = pos
    
    def get_x_pos(self, index):
        return self.x_pos[index]
    
    def get_y_pos(self):
        return self.y_pos