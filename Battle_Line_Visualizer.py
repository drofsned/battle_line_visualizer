""" 
    Battle Line Visualizer

    Game loop:
        - handle events
           - update game state
           - display game state

    'Card.py'       - loads and rezizes image files
    'Layout.py'     - defines/calculates x, y locations for the items on the display window
    'Row_Info.py'   - class used with layout
    'Table.py'      - displays the game state
    'State.py'      - maintains the game state
    'Controller.py' - controls the advance through the game data
    'Handler.py'    - handles the events and makes game state updates
    'Parser.py'     - parses the game data text 
    'Splash.py'     - display a pretty splash screen when the game starts
    'Utlities.py'   - global variables and utility functions

"""

import pygame, sys
from pygame.locals import *
from Handler import *

import splash

class Visualizer:
    def __init__(self, file):
        pygame.init()
        self.DISPLAYSURF = pygame.display.set_mode((1280, 960), RESIZABLE)
        pygame.display.set_caption('Battle Line')
        self.handler = Handler(file, self.DISPLAYSURF)

        splash.displaySplashScreen(self.DISPLAYSURF)
        #Flush the event log, opening a window triggers the VIDEORESIZE event, but want to see the spash screen
        pygame.event.clear()
        
    def run(self):
        while True: # main game loop
            for event in pygame.event.get():
                self.handler.handle(event, self.DISPLAYSURF)
            
            #once animations are added this will need the following here:
            #pygame.display.update()
            #pygame.time.Clock.tick(FPS) #where FPS is frames per second, eg FPS=60

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file = sys.argv[1]
    else:
        file = 'demo.txt'
    Visualizer(file).run()