import pygame
from pygame.locals import *
from Utilities import *
from Card import *
from Row_Info import *

class Layout(object):
    def __init__(self, surface):
        
        self.row_info = {
            'decks' :      Row_Info(NUM_DECK_CARDS),
            'hand-north' : Row_Info(NUM_HAND_CARDS),
            'hand-south':  Row_Info(NUM_HAND_CARDS),
            'flags':       Row_Info(NUM_COLUMNS),
            'line-north0': Row_Info(NUM_COLUMNS),
            'line-north1': Row_Info(NUM_COLUMNS),
            'line-north2': Row_Info(NUM_COLUMNS),
            'line-south0': Row_Info(NUM_COLUMNS),
            'line-south1': Row_Info(NUM_COLUMNS),
            'line-south2': Row_Info(NUM_COLUMNS)
        }
        
        self.__update_sizes(surface)
        self.__generate_positions()

    def __update_sizes(self, surface):
        self.__update_screen_size(surface)
        self.__update_card_size()
        self.__update_flag_size()
        self.__update_margins()
        self.__update_card_width()
    
    def __generate_positions(self):
        self.__update_x_pos(self.half_screen_width)
        self.__update_y_pos()
    
    def __update_screen_size(self, surface):
        self.screen_width = surface.get_width()
        self.screen_height = surface.get_height()

        self.half_screen_width = i_half(self.screen_width)
        self.half_screen_height = i_half(self.screen_height)
    
    def __update_card_size(self):
        self.card_height = self.__scaled_height(CARD_SCALE_FACTOR)
        dummy = Card()
        self.full_height = dummy.full_height
        dummy.shrink(self.getCardScale())
        self.card_width = dummy.width
        self.card_height = dummy.height
    
    def __update_flag_size(self):
        self.flag_height = self.__scaled_height(FLAG_SCALE_FACTOR)
        dummy_flag = Card('flag-white')
        self.flag_full_height = dummy_flag.full_height
        dummy_flag.shrink(self.getFlagScale())
        self.flag_width = dummy_flag.width
        self.flag_height = dummy_flag.height
        self.flag_height_half = i_round(self.flag_height/2)
    
    def __update_margins(self):
        self.row_info['decks'].set_x_margin(DECK_MARGIN)
        self.row_info['hand-north'].set_x_margin(1)
        self.row_info['hand-south'].set_x_margin(1)

        flag_x_margin = i_round(self.screen_width/10.0) - self.flag_width
        self.row_info['flags'].set_x_margin(flag_x_margin)
       
        line_x_margin = i_round(self.screen_width/10.0) - self.card_width
        for i in xrange(0, 3):
            self.row_info['line-north%i' % (i)].set_x_margin(line_x_margin)
            self.row_info['line-south%i'% (i)].set_x_margin(line_x_margin)
    
    def __update_card_width(self):
         for key in self.row_info:
            if key!='flags':
                self.row_info[key].set_width(self.card_width)
            else :
                self.row_info[key].set_width(self.flag_width)

    def __update_x_pos(self, half_screen_width):
        for key in self.row_info:
            self.row_info[key].set_x_pos(self.half_screen_width)
        
    def __update_y_pos(self):
        self.row_info['hand-north'].set_y_pos(self.__top_hand_y())
        self.row_info['hand-south'].set_y_pos(self.__bot_hand_y())
        self.row_info['flags'].set_y_pos(self.__flags_y())
        
        for i in xrange(0, 3):
            self.row_info['line-north%i' % (i)].set_y_pos(self.__line_top0_y() - i*self.__line_offset_y())
            self.row_info['line-south%i'% (i)].set_y_pos(self.__line_bot0_y() + i*self.__line_offset_y())
 
    def __scaled_height(self, scale):
        return i_round(scale*self.screen_height)
    
    def __top_hand_y(self):
        return 2*EDGE_MARGIN + self.card_height
        
    def __bot_hand_y(self):
        return self.screen_height-(self.card_height+EDGE_MARGIN)
    
    def __flags_y(self):
        return self.__top_hand_y() + self.card_height + i_round((self.__bot_hand_y() - (self.__top_hand_y()+self.card_height))/2) - self.flag_height_half

    def __line_top0_y(self):
        return self.__flags_y() - 2*EDGE_MARGIN - self.card_height
    
    def __line_bot0_y(self):
        return self.__flags_y() + self.flag_height + 2*EDGE_MARGIN
    
    def __line_offset_y(self):
        return i_round(self.card_height/5)
        
    def updateLayout(self, surface):
        self.__update_sizes(surface)
        self.__generate_positions()

    def getCardScale(self):
        return i_round(100.0*self.card_height/self.full_height)
    
    def getFlagScale(self):
        return i_round(100.0*self.flag_height/self.flag_full_height)
    
    def getLocations(self, name, index):
       return (self.row_info[name].get_x_pos(index), self.row_info[name].get_y_pos())
       
    def get_card_height(self):
        return self.card_height