from Utilities import *

class State(object):
    def __init__(self):
        self.north_bot = Player('BOT1')
        self.south_bot = Player('BOT2')
        self.flags = ['flag-white'] * NUM_COLUMNS

    def update(self, data):
        player = data[0]
        action = data[1]
        if len(data)>3:
            color = COLOR_LOOKUP[data[3].lower()]
        
        if action == 'is':
            if data[2].lower() == 'north':
                self.north_bot.rename(player)
            elif data[2].lower() == 'south':
                self.south_bot.rename(player)
        elif player == self.north_bot.name:
            bot = self.north_bot
        else:
            bot = self.south_bot
        
        if action == 'draws':
            if data[2].lower() != 'nothing':
                bot.add_card('%s-%s' % (color, data[2]))
        
        if action == 'plays':
            color = COLOR_LOOKUP[data[3].lower()]
            bot.remove_card('%s-%s' % (color, data[2]))
            bot.formations[int(data[4])-1].add_card('%s-%s' % (color, data[2]))
        
        if action == 'claims':
            if self.north_bot.name == player:
                self.flags[int(data[2]) - 1] = 'flag-blue'
            else:
                self.flags[int(data[2]) - 1] = 'flag-red'
 
    def reverse(self, data):
        
        player = data[0]
        action = data[1]
        if len(data)>3:
            color = COLOR_LOOKUP[data[3].lower()]
        
        if player == self.north_bot.name:
            bot = self.north_bot
        else:
            bot = self.south_bot
        
        if action == 'draws' and data[2].lower() != 'nothing':
            bot.remove_card('%s-%s' % (color, data[2]))

        if action == 'plays':
            bot.add_card('%s-%s' % (color, data[2]))
            bot.formations[int(data[4])-1].remove_card('%s-%s' % (color, data[2]))

        if action == 'claims':
            self.flags[int(data[2]) - 1] = 'flag-white'

class Player(object):
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.formations = [Formation() for _ in xrange(NUM_COLUMNS)]
    
    def rename(self, name):
        self.name = name
    
    def add_card(self, card):
        if len(self.hand) < NUM_HAND_CARDS:
            self.hand.append(card)
    
    def remove_card(self, card):
        if card in self.hand:
            self.hand.remove(card)

class Formation(object):
    def __init__(self):
        self.cards = []
    
    def add_card(self, card):
        if len(self.cards) < NUM_FORMATION_CARDS:
            self.cards.append(card)
    
    def remove_card(self, card):
        if card in self.cards:
            self.cards.remove(card)