# README #

Battle_Line_Visualizer.py

Run:
python battle_line_visualizer.py <filename>

Where <filename> is the name of the file with the game data you wish to visualize. If not provided, will default to "demo.txt"
