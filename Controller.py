import pygame, sys
from pygame.locals import *
from Utilities import *

class Controller(object):
    def __init__(self, surface):
        self.window = Window()
        self.button_names = ['forward', 'play', 'pause', 'back']
        self.text = '-- AI  Battle  Line --'
        self.__init_buttons(surface)
    
    def __init_buttons(self, surface):
        self.buttons = []
        for name in self.button_names:
            self.buttons.append(Button(name))
            
        for index, button in enumerate(self.buttons):
            button.set_rect_x(surface, button, index)
            button.set_rect_y(button)
        
    def __display_buttons(self, surface):
        for index, button in enumerate(self.buttons):
            surface.blit(button.image, (button.rect.x, button.rect.y))
            
    def __update_buttons(self, surface):
        for index, button in enumerate(self.buttons):
            button.set_rect_x(surface, button, index)

    def update_text(self, text):
        self.text = text
        
    def update(self, surface):
        self.window.update_x(surface.get_width())
        self.__update_buttons(surface)
	
    def display(self, surface):
        self.window.display_background(surface)
        self.window.display_outline(surface)
        self.window.display_text(surface, self.text)
        self.__display_buttons(surface)

    def buttonClicked(self, pos):
        clicked = 'none'
        for button in self.buttons:
            if button.rect.collidepoint(pos):
                clicked = button.name
        return clicked

class Window(object):
    def __init__(self):
        self.x = EDGE_MARGIN
        self.y = EDGE_MARGIN
        self.width = 300 - EDGE_MARGIN
        self.height = 21
        self.font = pygame.font.SysFont('Calibri', 18)
    
    def update_x(self, screen_width):
        self.x = screen_width - self.width - EDGE_MARGIN
    
    def display_background(self, surface):
        bg = pygame.Surface((self.width, self.height))
        bg.fill(WHITE)
        bg.set_alpha(190)
        surface.blit(bg, (self.x, self.y))
    
    def display_outline(self, surface):
        pygame.draw.rect(surface, GRAY, self.__get_rect(), 1)
    
    def display_text(self, surface, text):
        surface.blit(self.font.render(text, True, BLACK), (self.__offset(self.x), self.__offset(self.y)))
        
    def __get_rect(self):
        return pygame.Rect(self.x, self.y, self.width, self.height)

    def __offset(self, num):
        return num+3

class Button(object):
    def __init__(self, name):
        self.name = name
        self.image = pygame.image.load(imageFile(name)).convert()
        self.rect = self.image.get_rect()
  
    def set_rect_x(self, surface, button, index):
        button.rect.x = surface.get_width() - (index+1) * button.image.get_width() - (index+1)*EDGE_MARGIN
        
    def set_rect_y(self, button):
        button.rect.y = 21+2*EDGE_MARGIN