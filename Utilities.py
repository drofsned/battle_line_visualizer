"""
Global Definitions and Utility Functions
"""

#The Card Height should be roughly 14% of the window height to fit all
CARD_SCALE_FACTOR = 0.14
FLAG_SCALE_FACTOR = 0.05

NUM_DECK_CARDS = 2
NUM_HAND_CARDS = 7
NUM_COLUMNS = 9
NUM_FORMATION_CARDS = 3
EDGE_MARGIN = 5
DECK_MARGIN = 50

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GRAY = (128, 128, 128)
TAN = (243, 236, 226)

COLOR_LOOKUP = {
    'color1':   'red',
    'color2':   'yellow',
    'color3':   'green',
    'color4':   'blue',
    'color5':   'pink',
    'color6':   'white'
    }

def i_round(num):
    return int(num+0.5)

def i_half(num):
    return i_round(num/2)

def imageFile(name):
    return 'images/%s.png'%(name)