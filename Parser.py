import pygame
from pygame.locals import *

class Parser(object):
    def __init__(self, file):
        self.game_data=['Start']
        self.pointer = 0
        self.__load_game_data(file)

    def __load_game_data(self, file):
        
        try:
            with open(file, 'r') as f:
                for line in f:
                    self.game_data.append(line.rstrip())
        
        except IOError:
            self.game_data.append('**File Error**')

    def __increment_pointer(self):
        self.pointer += 1
        if self.pointer >= len(self.game_data):
            self.pointer = len(self.game_data)-1
            #force playback to stop
            pygame.time.set_timer(USEREVENT+1, 0)
    
    def __decrement_pointer(self):
        self.pointer -= 1
        if self.pointer < 0:
            self.pointer = 0
    
    def get_next_line(self):
        self.__increment_pointer()
        return self.game_data[self.pointer]

    def get_prev_line(self):
        self.__decrement_pointer()
        return self.game_data[self.pointer]
    
    def get_line_list(self):
        return self.game_data[self.pointer].split()