import pygame, sys
from pygame.locals import *
from Utilities import TAN
from Parser import *
from Table import *

class Handler(object):
    def __init__(self, file, surface):
        self.parser = Parser(file)
        self.table = Table(surface)

    def handle(self, event, surface):
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        
        elif event.type == VIDEORESIZE:
            surface = pygame.display.set_mode((event.w,event.h),RESIZABLE)
            self.table.resize(surface)
            self.table.display(surface)
        
        elif event.type == pygame.MOUSEBUTTONUP:
            button_clicked = self.table.controller.buttonClicked(pygame.mouse.get_pos())
            if button_clicked != 'none' :
                self.__handle_clicked_button(button_clicked)
            self.table.display(surface)
        
        elif event.type == pygame.USEREVENT+1:
            self.__handle_clicked_button('forward')
            self.table.display(surface)
        
        pygame.display.update()
        pygame.event.clear()

    def __handle_clicked_button(self, button):
        if button == 'pause':
            pygame.time.set_timer(USEREVENT+1, 0)
        
        elif button == 'play':
            pygame.time.set_timer(USEREVENT+1, 100)
        
        elif button == 'forward':
            self.table.controller.update_text(self.parser.get_next_line())
            self.table.state.update(self.parser.get_line_list())
            self.table.update()

        elif button == 'back':
            if self.parser.pointer>0:
                self.table.state.reverse(self.parser.get_line_list())
                self.table.controller.update_text(self.parser.get_prev_line())
                self.table.update()