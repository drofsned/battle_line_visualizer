import pygame, sys
from pygame.locals import *

TAN = (243, 236, 226)
BLACK =(0, 0, 0)

def displaySplashScreen(surface):
    surface.fill(TAN)
    width = surface.get_width()
    height = surface.get_height()
    
    logo = pygame.image.load('images/logo.png').convert()
    logo_width = logo.get_width()
    logo_height = logo.get_height()
    
    logo_x = int((width/2)-(logo_width/2))
    logo_y = int((height/2)-(logo_height/2))
    
    font = pygame.font.SysFont('Calibri', 20)
    surface.blit(font.render('<Click to begin>', True, BLACK), (10, 10))
    surface.blit(logo, (logo_x, logo_y))
    pygame.display.update()