import pygame
from pygame.locals import *
from Utilities import *
from Card import *
from State import *
from Layout import *
from Controller import Controller

class Table(object):
    def __init__(self, surface):
        self.state = State()
        self.layout = Layout(surface)
        self.controller = Controller(surface)
        self.controller.update(surface)
        self.info_card = Card('info1', 30)
        
        self.rows = {
            'decks': [Card('card-troop'), Card('card-tactics')],
            'hand-north': [],
            'hand-south': [],
            'flags': [Card('flag-white', self.layout.getFlagScale())] * NUM_COLUMNS
        }
        
        self.line_north = []
        self.line_south = []
    
    def display(self, surface):
        surface.fill(TAN)
        self.__display_info_card(surface)
        for row in self.rows:
            self.__display_rows(surface, row)
        self.__display_line_cards(surface)
        self.controller.display(surface)
        
    def resize(self, surface):
        self.layout.updateLayout(surface)
        self.controller.update(surface)
        self.update()
  
    def __display_info_card(self, surface):
        surface.blit(self.info_card.image, (EDGE_MARGIN, EDGE_MARGIN))
    
    def __display_rows(self, surface, row):
        for index, card in enumerate(self.rows[row]):
            pos = self.layout.getLocations(row, index)
            surface.blit(card.image, (pos[0], pos[1]))

    def __display_line_cards(self, surface):
        for colIndex, col in enumerate(self.line_north):
            for index, card in enumerate(col):
                if card != 'empty':
                    pos = self.layout.getLocations('line-north'+str(index), colIndex)
                    surface.blit(card.image, (pos[0], pos[1]))

        for colIndex, col in enumerate(self.line_south):
            for index, card in enumerate(col):
                if card != 'empty':
                    pos = self.layout.getLocations('line-south'+str(index), colIndex)
                    surface.blit(card.image, (pos[0], pos[1]))
    
    def update(self):
        self.rows['decks'][0].shrink(self.layout.getCardScale())
        self.rows['decks'][1].shrink(self.layout.getCardScale())
        
        del self.rows['hand-north'][:]
        for name in self.state.north_bot.hand:
            self.rows['hand-north'].append(Card(name, self.layout.getCardScale()))
        
        del self.rows['hand-south'][:]
        for name in self.state.south_bot.hand:
            self.rows['hand-south'].append(Card(name, self.layout.getCardScale()))
        
        del self.rows['flags'][:]
        for name in self.state.flags:
            self.rows['flags'].append(Card(name, self.layout.getFlagScale()))
        
        del self.line_north[:]
        self.line_north = [[Card(name, self.layout.getCardScale()) for name in formation.cards] for formation in self.state.north_bot.formations]
        
        del self.line_south[:]
        self.line_south = [[Card(name, self.layout.getCardScale()) for name in formation.cards] for formation in self.state.south_bot.formations]